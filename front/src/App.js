import React from "react";
import { Toaster } from "react-hot-toast";
import { Header, Grid } from "semantic-ui-react";
import { LinkTable } from "./features/table/LinkTable";

import "semantic-ui-css/semantic.min.css";
import "./App.css";

function App() {
  return (
    <Grid
      className="App"
      relaxed
      padded="horizontally"
      columns={1}
      centered
      as="main"
      verticalAlign="middle"
    >
      <Grid.Row verticalAlign="top">
        <Grid.Column textAlign="center">
          <Header as="h1">URL Shortener</Header>
          <LinkTable />
        </Grid.Column>
      </Grid.Row>
      <Toaster />
    </Grid>
  );
}

export default App;
