import axios from "axios";

export async function makeShort(url) {
  try {
    const response = await axios({
      method: "post",
      url: `${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/shortener/v1`,
      data: {
        url,
      },
      validateStatus: function (status) {
        return status === 303 || status === 200;
      },
    });

    return response;
  } catch (err) {
    console.error("API ERROR:", err.response);
    throw Error(err.response.data.error);
  }
}

export async function delShort(id) {
  console.debug("deleteing id:", id);
  try {
    const response = await axios({
      method: "delete",
      url: `${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/shortener/v1/${id}`,
    });
    return response;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

export async function getPage(page, per_page) {
  try {
    const response = await axios({
      method: "get",
      url: `${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}/shortener/v1`,
      params: { page, per_page },
    });
    return response.data;
  } catch (err) {
    console.error(err);
    throw err;
  }
}
