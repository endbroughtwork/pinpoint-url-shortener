import tableLinkReducer, { setDelState, setMakeState } from "./tableSlice";

describe("tableLink reducer", () => {
  const initialState = {
    page: 1,
    per_page: 5,
    total: 0,
    data: [],
    del: "idle",
    make: "idle",
    get: "idle",
  };

  it("should handle initial state", () => {
    expect(tableLinkReducer(undefined, { type: "unknown" })).toEqual({
      page: 1,
      per_page: 5,
      total: 0,
      data: [],
      del: "idle",
      make: "idle",
      get: "idle",
    });
  });

  it("should handle setDelState", () => {
    const actual = tableLinkReducer(initialState, setDelState("done"));
    expect(actual.del).toEqual("done");
  });

  it("should handle setMakeState", () => {
    const actual = tableLinkReducer(initialState, setMakeState("done"));
    expect(actual.make).toEqual("done");
  });
});
