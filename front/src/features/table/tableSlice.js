import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { makeShort, delShort, getPage } from "./linkAPI";
import toast from "react-hot-toast";

const initialState = {
  page: 1,
  per_page: 5,
  total: 0,
  data: [],
  del: "idle",
  make: "idle",
  get: "idle",
};

export const triggerMake = createAsyncThunk(
  "linkTable/makeShort",
  async (target_url) => {
    const promise = makeShort(target_url);
    toast.promise(
      promise,
      {
        loading: "Creating...",
        success: "Made shorty.",
        error: "Creation failed.",
      },
      {
        style: {
          minWidth: "250px",
        },
      }
    );
    await promise;
    return;
  }
);

export const triggerDel = createAsyncThunk("linkTable/delShort", async (id) => {
  const promise = delShort(id);
  toast.promise(
    promise,
    {
      loading: "Deleting...",
      success: "Deleted shorty.",
      error: "Deletion failed.",
    },
    {
      style: {
        minWidth: "250px",
      },
    }
  );
  await promise;
  return;
});

export const triggerGet = createAsyncThunk(
  "linkTable/getPage",
  async ({ page, per_page }) => {
    const promise = getPage(page, per_page);
    toast.promise(
      promise,
      {
        loading: "Loading...",
        success: "Loaded shorty table.",
        error: "Loading URLs failed.",
      },
      {
        style: {
          minWidth: "250px",
        },
      }
    );
    const response = await promise;
    return response;
  }
);

export const linkTableSlice = createSlice({
  name: "linkTable",
  initialState,
  reducers: {
    setDelState: (state, action) => {
      state.del = action.payload;
    },
    setMakeState: (state, action) => {
      state.make = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(triggerMake.pending, (state) => {
        state.make = "pending";
      })
      .addCase(triggerMake.fulfilled, (state) => {
        state.make = "done";
      })
      .addCase(triggerMake.rejected, (state) => {
        state.make = "error";
      })
      .addCase(triggerDel.pending, (state) => {
        state.del = "pending";
      })
      .addCase(triggerDel.fulfilled, (state) => {
        state.del = "done";
      })
      .addCase(triggerDel.rejected, (state) => {
        state.del = "error";
      })
      .addCase(triggerGet.pending, (state) => {
        state.get = "pending";
      })
      .addCase(triggerGet.fulfilled, (state, action) => {
        const { data, total, per_page, page } = action.payload;
        state.get = "done";
        state.data = data;
        state.page = page;
        state.total = total;
        state.per_page = per_page;
      })
      .addCase(triggerGet.rejected, (state, action) => {
        state.get = "error";
      });
  },
});

export const { setDelState, setMakeState } = linkTableSlice.actions;

export function selectMakeWaiting({ linkTable }) {
  return linkTable.make;
}

export function selectDelWaiting({ linkTable }) {
  return linkTable.del;
}

export function selectGetWaiting({ linkTable }) {
  return linkTable.get;
}

export function selectTableData({ linkTable }) {
  return linkTable.data;
}

export function selectTotalEntries({ linkTable }) {
  return linkTable.total;
}

export function selectPerPage({ linkTable }) {
  return linkTable.per_page;
}

export function selectActivePage({ linkTable }) {
  return linkTable.page;
}

export default linkTableSlice.reducer;
