import { makeShort, delShort, getPage } from "./linkAPI";

describe("API TESTS", () => {
  beforeAll(() => {
    console.error = jest.fn();
  });

  afterAll(() => {
    console.error.mockClear();
  });
  const testUrl = "http://google.com";
  const expectedNew = {
    id: expect.any(Number),
    short_code: expect.any(String),
    url: expect.any(String),
    created_at: expect.any(String),
  };
  let existing = {};
  it("make should return with good body on new item", async () => {
    const response = await makeShort(testUrl);
    expect(response.data).toMatchObject(expectedNew);
    expect(response.status).toEqual(200);
    existing = response.data;
  });

  it("make should return with good body on existing item", async () => {
    const response = await makeShort(testUrl);
    expect(response.data).toMatchObject(existing);
    expect(response.status).toEqual(303);
  });

  it("make should throw for bad URL", async () => {
    await expect(makeShort("badurl")).rejects.toThrow(
      "Target URL is not a valid URL"
    );
  });

  it("get should return 200 on good request", async () => {
    const response = await getPage(1, 5);
    expect(response).toMatchObject({
      page: 1,
      per_page: 5,
      total: 1,
      data: [expectedNew],
    });
  });

  it("del should return with good status on existing item", async () => {
    const response = await delShort(1);
    expect(response.status).toEqual(204);
    existing = response.data;
  });

  it("del should return with good status on non-existent item", async () => {
    const response = await delShort(1);
    expect(response.status).toEqual(204);
    existing = response.data;
  });

  it("del should throw on errors", async () => {
    await expect(delShort(null)).rejects.toThrow();
  });
});
