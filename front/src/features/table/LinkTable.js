import React, { useEffect, useState, useCallback } from "react";
import dayjs from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
import utc from "dayjs/plugin/utc";
import {
  Button,
  Dimmer,
  Dropdown,
  Icon,
  Loader,
  Pagination,
  Table,
} from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { nanoid } from "nanoid";
import {
  triggerMake,
  selectDelWaiting,
  selectGetWaiting,
  selectMakeWaiting,
  selectTableData,
  selectTotalEntries,
  selectActivePage,
  selectPerPage,
  setDelState,
  setMakeState,
  triggerDel,
  triggerGet,
} from "./tableSlice";
import { MakeModal } from "../makeModal/MakeModal";
dayjs.extend(utc);
dayjs.extend(localizedFormat);

const perPageOptions = [
  { key: "5", text: "5", value: 5 },
  { key: "10", text: "10", value: 10 },
  { key: "15", text: "15", value: 15 },
  { key: "20", text: "20", value: 20 },
];

export function LinkTable(props) {
  const [modalOpen, setModalOpen] = useState(false);
  const makeWaiting = useSelector(selectMakeWaiting);
  const delWaiting = useSelector(selectDelWaiting);
  const getWaiting = useSelector(selectGetWaiting);
  const tableData = useSelector(selectTableData);
  const activePage = useSelector(selectActivePage);
  const totalEntries = useSelector(selectTotalEntries);
  const entriesPerPage = useSelector(selectPerPage);
  const dispatch = useDispatch();
  const handleDeleteClick = (id) => () => dispatch(triggerDel(id));
  const handleMakeClick = () => setModalOpen(true);
  const handlePerPageChange = useCallback(
    (evt, data) => {
      dispatch(triggerGet({ page: 1, per_page: data.value }));
    },
    [dispatch]
  );
  const handlePaginationChange = useCallback(
    (_, next_state) => {
      dispatch(
        triggerGet({ page: next_state.activePage, per_page: entriesPerPage })
      );
    },
    [dispatch, entriesPerPage]
  );
  useEffect(() => {
    if (getWaiting === "idle")
      dispatch(triggerGet({ page: 1, per_page: entriesPerPage }));
    if (delWaiting === "done") {
      dispatch(triggerGet({ page: 1, per_page: entriesPerPage }));
      dispatch(setDelState("idle"));
    }
    if (makeWaiting === "done") {
      console.log("trigger make");
      dispatch(triggerGet({ page: 1, per_page: entriesPerPage }));
      dispatch(setMakeState("idle"));
    }
  }, [dispatch, getWaiting, delWaiting, makeWaiting, entriesPerPage]);

  return (
    <>
      <Dimmer active={getWaiting === "pending"}>
        <Loader />
      </Dimmer>
      <MakeModal
        open={modalOpen}
        setModalOpen={setModalOpen}
        triggerMake={triggerMake}
      />
      <Table compact celled definition>
        <Table.Header>
          <Table.Row></Table.Row>
        </Table.Header>

        <Table.Header>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Target URL</Table.HeaderCell>
            <Table.HeaderCell>Short URL</Table.HeaderCell>
            <Table.HeaderCell>Created</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {tableData.map((item) => (
            <Table.Row key={nanoid(4)}>
              <Table.Cell collapsing>
                <Button
                  loading={delWaiting === "pending"}
                  color="red"
                  animated="vertical"
                  onClick={handleDeleteClick(item.id || -1)}
                >
                  <Button.Content hidden>Delete</Button.Content>
                  <Button.Content visible>
                    <Icon name="trash" />
                  </Button.Content>
                </Button>
              </Table.Cell>
              <Table.Cell>{item.id}</Table.Cell>
              <Table.Cell>{item.url}</Table.Cell>
              <Table.Cell>
                <Button
                  href={`http://localhost:5000/v1/${
                    item.short_code.split("/")[1]
                  }`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Go!
                </Button>
                {item.short_code}
              </Table.Cell>
              <Table.Cell>
                {dayjs(item.created_at).local().format("llll")}
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>

        <Table.Footer fullWidth>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell colSpan="3">
              <Pagination
                activePage={activePage}
                boundaryRange={1}
                onPageChange={handlePaginationChange}
                size="mini"
                siblingRange={1}
                totalPages={Math.ceil(totalEntries / entriesPerPage)}
              />
              <span> </span>
              <Dropdown
                placeholder="Entries per Page"
                options={perPageOptions}
                value={entriesPerPage}
                onChange={handlePerPageChange}
              />
            </Table.HeaderCell>
            <Table.HeaderCell colSpan="1">
              <Button
                loading={makeWaiting === "pending"}
                floated="right"
                icon
                labelPosition="left"
                primary
                size="small"
                animated="vertical"
                onClick={handleMakeClick}
              >
                <Button.Content hidden>Make New Shorty</Button.Content>
                <Button.Content visible>
                  <Icon name="linkify" />
                </Button.Content>
              </Button>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </>
  );
}
