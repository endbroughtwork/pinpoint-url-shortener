import React, { useState, useCallback } from "react";
import { Button, Modal, Input } from "semantic-ui-react";
import { useDispatch } from "react-redux";

export function MakeModal({ open, setModalOpen, triggerMake }) {
  const [targetURL, setTargetURL] = useState("");
  const dispatch = useDispatch();
  const handleClearState = () => {
    setTargetURL("");
  };
  const handleClose = useCallback(() => {
    setModalOpen(false);
  }, [setModalOpen]);
  const handleMakeClick = useCallback(() => {
    dispatch(triggerMake(targetURL));
    handleClose();
  }, [targetURL, triggerMake, handleClose, dispatch]);
  const handleTextInput = useCallback(
    (evt) => setTargetURL(evt.target.value),
    [setTargetURL]
  );
  const handleKey = (evt) => {
    if (evt.code === "Enter") handleMakeClick();
  };
  return (
    <Modal size="small" open={open} onClose={handleClearState}>
      <Modal.Header>Make a Short URL</Modal.Header>
      <Modal.Content>
        <Input
          fluid
          labelPosition="left"
          type="text"
          placeholder="Full Link"
          label="URL"
          onChange={handleTextInput}
          onKeyPress={handleKey}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button negative onClick={handleClose}>
          Cancel
        </Button>
        <Button disabled={targetURL === ""} positive onClick={handleMakeClick}>
          Make It
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
