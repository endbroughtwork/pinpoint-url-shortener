/**
 * @jest-environment jsdom
 */
import React from "react";
import { screen, render, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../../app/store";
import { MakeModal } from "./MakeModal";

describe("renders App", () => {
  let mockProps = {};
  beforeAll(() => {
    mockProps = {
      open: true,
      setModalOpen: jest.fn(),
      triggerMake: jest.fn(),
    };
  });

  it("should have a title", () => {
    render(
      <Provider store={store}>
        <MakeModal {...mockProps} />
      </Provider>
    );
    expect(screen.getByText(/Make a Short URL/i)).toBeInTheDocument();
  });

  it("should call setModalOpen when clicking cancel", () => {
    render(
      <Provider store={store}>
        <MakeModal {...mockProps} />
      </Provider>
    );
    fireEvent.click(screen.getByText(/Cancel/i));
    expect(mockProps.setModalOpen).toHaveBeenCalledTimes(1);
  });

  it("should call setModalOpen when clicking cancel", () => {
    render(
      <Provider store={store}>
        <MakeModal {...mockProps} />
      </Provider>
    );
    fireEvent.click(screen.getByText(/Cancel/i));
    expect(mockProps.setModalOpen).toHaveBeenCalledTimes(1);
  });

  it("should update value on input change", () => {
    render(
      <Provider store={store}>
        <MakeModal {...mockProps} />
      </Provider>
    );
    expect(screen.getByPlaceholderText(/Full Link/i)).toHaveValue("");
    fireEvent.change(screen.getByPlaceholderText(/Full Link/i), {
      target: {
        value: "while",
      },
    });
    expect(screen.getByPlaceholderText(/Full Link/i)).toHaveValue("while");
  });

  it("should clear targetURL when closed", () => {
    const { rerender } = render(
      <Provider store={store}>
        <MakeModal {...mockProps} />
      </Provider>
    );
    fireEvent.change(screen.getByPlaceholderText(/Full Link/i), {
      target: {
        value: "while",
      },
    });
    expect(screen.getByPlaceholderText(/Full Link/i)).toHaveValue("while");
    rerender(
      <Provider store={store}>
        <MakeModal {...mockProps} open={false} />
      </Provider>
    );
    rerender(
      <Provider store={store}>
        <MakeModal {...mockProps} />
      </Provider>
    );
    expect(screen.getByPlaceholderText(/Full Link/i)).toHaveValue("");
  });
});
