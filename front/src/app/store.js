import { configureStore } from "@reduxjs/toolkit";
import linkTableReducer from "../features/table/tableSlice";

export const store = configureStore({
  reducer: {
    linkTable: linkTableReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});
