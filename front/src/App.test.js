import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "./app/store";
import App from "./App";

describe("renders App", () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  it("should have a title", () => {
    expect(getByText(/URL Shortener/i)).toBeInTheDocument();
  });
});
