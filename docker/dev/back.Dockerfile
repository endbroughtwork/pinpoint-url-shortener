FROM python:3.9

WORKDIR /usr/src/app
COPY requirements.txt .
RUN pip install --no-cache-dir -I -r requirements.txt
