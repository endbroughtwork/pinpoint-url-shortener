FROM node:gallium-alpine as build

WORKDIR /home/node/app
COPY package.json .
COPY src src
COPY public public
COPY yarn.lock .
COPY .env.prod .env
RUN yarn install && yarn build

FROM nginx
COPY --from=build /home/node/app/build /usr/share/nginx/html
