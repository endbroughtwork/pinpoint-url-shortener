FROM python:3.9

WORKDIR /usr/src/app
COPY requirements.txt .
COPY main.py .
COPY prod.cfg .
COPY utils utils
COPY routes routes
COPY migrations migrations
COPY db db

RUN pip install --no-cache-dir -I -r requirements.txt
