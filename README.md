# pinpoint-url-shortener

## Description
A toy URL shortener appplication. It consists of a Postgresql 11 DB, a Python Flask backend server for REST functionality, and a Javascript React frontend.

## Badges
[![pipeline status](https://gitlab.com/endbroughtwork/pinpoint-url-shortener/badges/main/pipeline.svg)](https://gitlab.com/endbroughtwork/pinpoint-url-shortener/-/commits/main)
[![coverage report](https://gitlab.com/endbroughtwork/pinpoint-url-shortener/badges/main/coverage.svg)](https://gitlab.com/endbroughtwork/pinpoint-url-shortener/-/commits/main)


## Usage
### Dev server
```bash
cd front;
cp .env.dev .env;
npm install;
cd ..;
docker-compose -f docker/dev/compose.yml up -d;
```
Development servers for backend and frontend use bind mounts to the front and back folders. Consequently, you can edit the files on the host and still have hot reloading available in the guests. Do not attempt to run Dev and Prod clusters at the same time. It will cause port conflicts. Frontend is served on `http://localhost:3000`. Backend is served on `http://localhost:5000`. Postgresql is served at `postgres://postgres:postgres@localhost:6543/ppt`.
### Prod server
```bash
docker-compose -f docker/prod/compose.yml up -d;
```
Production frontend builds and minifies the webapp then copies it to an nginx container served at `http://localhost:8080`. Production backend serves the flask app using gunicorn on `http://localhost:5000`. Production Postgresql exposes `5432` to other services and exposes `6543` to the host.

## License
This project is licensed under the best license. The Only License. The WTFPL.

## Project status
Mostly complete. Plenty of room for improvements:
* Add a Redis DB container and configure for caching requests to Postgres
* Add Kafka, RabbitMQ, or to handle asynchronous shorty creation
* Drop Postgresql entirely and use an in-memory DB (assuming this stays a toy)
