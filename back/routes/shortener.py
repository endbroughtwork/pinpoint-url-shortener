from flask import current_app, Blueprint, request, jsonify
from flask_cors import CORS
from db import db
from db.translation import Translation
short_v1 = Blueprint('short', __name__, url_prefix='/shortener')
CORS(short_v1)



@short_v1.route('v1', methods=['GET', 'POST'])
@short_v1.route('v1/<int:url_id>', methods=['DELETE'])
def dispatch(url_id=None):
    """Handle the dispatch of operations
    """
    if request.method == 'POST':
        body = request.get_json()
        return handle_post(body["url"])
    if request.method == 'GET':
        try:
            return handle_get(int(request.args.get('page')), int(request.args.get('per_page')))
        except:
            return '', 500
    if request.method == 'DELETE':
        return handle_delete(url_id)


#  GET /shortener
#  Request Query Parameters
#      page (Integer) -- Page to be returned
#      per_page (Integer) -- Number of records per page
#  Response Body
#      List of object with following schema
#              id (Integer)
#              url (String)
#              short_code (String)
#              created_at (Date Time)
#          page (Integer) -- The current page
#          pages (Integer) -- Total pages
#          per_page (Integer) -- Number of records per page
#          total (Integer) -- Total records
def handle_get(page=1, per_page=5):
    """Handle GET on shortener
    :page: int page for pagination
    :per_page: int number of items per page
    :returns: list of objects
    """
    results = Translation.query.order_by(Translation.dt.desc()).paginate(page=page, per_page=per_page, error_out=True).items
    out = [{
        'id': result.id,
        'url': result.proto + result.target_url,
        'short_code': result.short_url,
        'created_at': result.dt
    } for result in results]
    current_app.logger.debug("Results: %s" % out)
    return { 'data': out, 'page': page, 'per_page': per_page, 'total': Translation.count() }, 200


#  POST /shortener
#  Request Body
#      URL (String)
#  Response Body
#      id(Integer)
#      url (String)
#      short_code (String)
#      created_at (Date Time)
def handle_post(target_url):
    """Handle POST on shortener
    :target_url: string URL to be shortened
    :returns: HTTP 303 + JSON body for existing entry
              HTTP 200 + JSON body for new entry
              HTTP 400 on invalid url scheme
    """
    current_app.logger.debug("target_url = %s" % target_url)
    code, output = Translation.create(target_url)
    if code == 400:
        return output, 400
    (id, target, short, proto, dt) = output
    response_body = {
        'id': id,
        'url': "{}{}".format(proto if proto is not None else '', target),
        'short_code': short,
        'created_at': dt
    }
    return response_body, code


#  DELETE /shortener/<id>
#  Request Path Parameter
#      id (Integer) -- The id to be deleted
#  Response:
#      <Empty>
def handle_delete(url_id):
    """Handle DELETE on shortener
    :url_id: int ID to delete from DB
    :returns: TODO
    """
    current_app.logger.info("Deleting %d" % url_id)
    try:
        Translation.delete(url_id)
    except Exception as e:
        current_app.logger.error("Encountered problem: %s" % e)
        return { 'error': str(e) }, 500
    # NOTE: According to https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.7
    # HTTP 204 is the appropriate response for a successful delete that returns no body
    return '', 204
