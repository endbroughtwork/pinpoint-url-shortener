from flask import current_app, Blueprint, request, jsonify, redirect
from flask_cors import CORS
from db import db
from db.translation import Translation
root_v1 = Blueprint('root', __name__, url_prefix='/')
CORS(root_v1)

#  GET /<code>
#      Request Path Parameter
#          code (String)
#  Response
#      Http Status
#          301
#      Header
#          Location: <Original URL>
@root_v1.route('v1/<string:code>', methods=['GET'])
def dispatch(code=None):
    """Handle the dispatch of operations
    :code: string the code to search for
    :returns: HTTP 301 redirect to target_url
              HTTP 404 if code doesn't map to a target
              HTTP 500 if no handlers returned anything
    """
    if code is None:
        return '', 404
    if request.method == 'GET':
        return handle_get(code)

def handle_get(code):
    """Find and redirect target_url from code

    :code: string the code to search for
    :returns: HTTP 301 redirect to target_url
              HTTP 404 if code doesn't map to a target
              HTTP 500 on error
    """
    try:
        code, entry = Translation.find_by_code(code)
        if code == 404:
            return '', 404
        (_, target, _, proto, _) = entry
        current_app.logger.debug("Entry found %s and %s" % (target, proto))
        # if proto is blank, defautl https
        location = (proto if proto != '' else 'https://') + target
        current_app.logger.debug('location %s '% location)
        return redirect(location, code=301)
    except Exception as e:
        return { 'error': str(e) }, 500
