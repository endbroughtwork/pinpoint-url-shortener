from flask import current_app
from random import choices
import re
from string import ascii_uppercase, ascii_lowercase, digits
# Putting them here since I don't need the extra files yet.

url_val = re.compile(r"(^.*://)?((.*)\.(.*$)){1}", flags=re.IGNORECASE)

def make_url(domain):
    """Make a short URL using the domain as base

    :domain: string
    :returns: string

    """

    return "{}/{}".format(domain, ''.join(choices(ascii_uppercase + digits + ascii_lowercase, k=10)))

def strip_proto(target_url):
    """Strip protocol from the start of the URL

    :target_url: string URL to clean
    :returns: match groups for proto and the rest of the URL

    """
    matched = url_val.match(target_url)
    return matched.group(1), matched.group(2).rstrip('/')

def validate_url(target_url):
    """Validate the the url at least matched [proto](domain)(tld)
    Will match fake URLS, but that's fine for a toy

    """
    matched = url_val.fullmatch(target_url)

    if matched is None:
        raise Exception("Target URL is not a valid URL")

