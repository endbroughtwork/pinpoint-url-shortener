from flask import Flask, abort, jsonify
from flask_migrate import Migrate
from logging.config import dictConfig

from routes.shortener import short_v1
from routes.root import root_v1
from db import db

migrate = Migrate()

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


def resource_not_found(e):
    return jsonify(error=str(e)), 404


def create_app():
    app = Flask(__name__)
    app.config.from_envvar('SETTINGS')
    app.register_blueprint(short_v1)
    app.register_blueprint(root_v1)
    db.init_app(app)
    migrate.init_app(app, db)
    with app.app_context():
        db.create_all()
    return app
