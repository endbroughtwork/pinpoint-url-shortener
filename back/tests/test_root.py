import pytest
from conftest import app

def test_root_bad_http_method(client):
    resp = client.post('/v1')
    assert resp.status_code == 404

def test_root_bad_request(client):
    resp = client.get('/v1/fail')
    assert resp.status_code == 404

def test_root_good_request(client):
    # Make (or get and entry) an entry
    resp = client.post("/shortener/v1", json={
        'url': "https://google.com"
    })
    code = resp.json['short_code'].split('/')[1]
    print(code)
    resp = client.get("/v1/{}".format(code))
    assert resp.status_code == 301
    assert resp.location == 'https://google.com'
