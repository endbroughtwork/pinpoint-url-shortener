from utils.utils import make_url,strip_proto
from os import environ

domain = 're.re'

def test_make_url_length():
    out = make_url(domain)
    assert len(out) == 16
    assert out[0:6] == domain+ '/'

def test_make_url_domainpart():
    out = make_url(domain)
    assert out[0:6] == domain+ '/'

def test_strip_proto_has_proto():
    proto, target = strip_proto("https://google.com")
    assert proto == "https://"
    assert target == "google.com"

def test_strip_proto_no_proto():
    proto, target = strip_proto("google.com")
    assert proto == None
    assert target == "google.com"
