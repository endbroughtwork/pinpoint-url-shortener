import pytest
from conftest import app

def test_short_bad_get(client):
    resp = client.get('/shortener/v1')
    assert resp.status_code == 500


def test_short_good_get(client):
    resp = client.get('/shortener/v1?page=1&per_page=5')
    assert resp.status_code == 200

def test_short_good_post(client):
    resp = client.post("/shortener/v1", json={
        'url': "https://ddg.gg"
    })
    assert resp.status_code == 200

def test_short_good_post_repeat(client):
    resp = client.post("/shortener/v1", json={
        'url': "https://ddg.gg"
    })
    assert resp.status_code == 303

def test_short_bad_post(client):
    resp = client.post("/shortener/v1", json={
        'url': "asdf"
    })
    assert resp.status_code == 400

def test_short_good_delete(client):
    resp = client.post("/shortener/v1", json={
        'url': "https://ddg.gg"
    })
    id = resp.json['id']
    resp = client.delete("/shortener/v1/{}".format(id))
    assert resp.status_code == 204

def test_short_good_delete_repeat(client):
    resp = client.post("/shortener/v1", json={
        'url': "https://ddg.gg"
    })
    id = resp.json['id']
    resp = client.delete("/shortener/v1/{}".format(id))
    assert resp.status_code == 204

