from datetime import datetime
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy import delete
from flask import current_app
from flask_sqlalchemy import BaseQuery
from utils.utils import validate_url, strip_proto, make_url
from . import db



class Translation(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    target_url = db.Column(db.String(2048), index=True)
    short_url = db.Column(db.String(16), index=True)
    proto = db.Column(db.String(16))
    dt = db.Column(db.DateTime)
    __table_args__ = (
        db.UniqueConstraint('proto', 'target_url', name="unq_trs_proto_target"),
    )

    @staticmethod
    def create(target_url):
        try:
            validate_url(target_url)
        except Exception as e:
            return 400, { 'error': str(e) }
        # create random string
        proto, clean_target = strip_proto(target_url)
        proto = proto if proto is not None else ''
        current_app.logger.debug("proto %s - target %s" % ( proto, clean_target ))
        short_url = make_url(current_app.config['SHORT_DOMAIN'])
        stmt = insert(Translation).values(
            {
                'target_url': clean_target,
                'short_url': short_url,
                'proto': proto,
                'dt': datetime.utcnow()
            }
        ).on_conflict_do_nothing(
            constraint='unq_trs_proto_target'
        ).returning(Translation)
        results = db.session.execute(stmt)
        db.session.commit()
        for result in results.fetchall():
            current_app.logger.info("inserted or updated: %s" % result)
            return 200, result
        results = Translation.query.where(Translation.target_url == clean_target, Translation.proto == proto).limit(1).all()
        for result in results:
            current_app.logger.info("found previously created: %s" % result)
            # NOTE: According to https://www.rfc-editor.org/rfc/rfc7231#section-6.4.4 the correct status
            # code for a POST that does not create but returns the previous created value is
            # 303 SEE OTHER
            return 303, result.to_tuple()

    @staticmethod
    def count():
        """Retrieve and count of rows in the table
        :returns: int count of rows
        """
        return db.session.query(db.func.count(Translation.id)).scalar()

    @staticmethod
    def delete(url_id):
        """Delete an entry from the DB

        :url_id: int the ID of the item to be deleted
        :returns: None

        """
        stmt = delete(Translation).where(Translation.id == url_id).execution_options(synchronize_session="evaluate")
        results = db.session.execute(stmt)
        db.session.commit()
        return True

    @staticmethod
    def find_by_code(code):
        """Find a Translation entry by code

        :code: TODO
        :returns: TODO

        """
        try:
            result = Translation.query.where(
                Translation.short_url == "{}/{}".format(
                    current_app.config['SHORT_DOMAIN'],
                    code
                )).limit(1).all()
            return 200, result[0].to_tuple()
        except Exception:
            return 404, None

    def to_tuple(self):
        return (self.id, self.target_url, self.short_url, self.proto, self.dt)

    def __str__(self):
        return "{} | {} | {} | {} @ \n {}".format(self.id, self.proto, self.target_url, self.short_url, self.dt)

    def __repr__(self):
        return "{} | {} | {} | {} @ \n {}".format(self.id, self.proto, self.target_url, self.short_url, self.dt)
